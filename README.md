# Text+ Registry Models

Repository zur Sicherung der Datenmodelle, Vokabulare und Ressourcenbeschreibungen der Text+ Registry. Inhalte werden im Moment manuell gepflegt. Die Registry wird jedoch zukünfig Änderungen am Datenbestand auch automatisch in dieses Projekt publizieren.
